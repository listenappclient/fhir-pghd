from flask import Flask, request
from flask_restful import Resource, Api, reqparse
import json
import os
from datetime import datetime, timedelta

def checkResourceByCategory(resource: dict, category: str) -> bool:
	categories = resource['category']
	for cat in categories:
		for c in cat['coding']:
			if c['code'] == category:
				return True
	return False

def checkResourceByCode(resource: dict, code: str) -> bool:
	codes = resource['code']['coding']
	for c in codes:
		if c['code'] == code:
			return True
	return False

def addIdEndpoints(api):
	# observations
	# inp_path = '/mnt/c/Users/gilor/Documents/ETL/FHIR/FHIR-IMNA/Profiles/Observation/observation.json'
	inp_path = './fhir-api/imna_observations.json'
	with open(file=inp_path, mode='r') as jsonFile:
		data = json.load(jsonFile)
		jsonFile.close()
	ids = set([entry['resource']['id'] for entry in data['entry']])
	for id in ids:
		print("adding %s", {id})
		api.add_resource(Observation, f'/Observation/{id}', endpoint=f'Observation/{id}', resource_class_kwargs={'id':id})
#	with open(file='/mnt/c/Users/gilor/Documents/ETL/FHIR/FHIR-IMNA/Profiles/Bundle/bundle.json', mode='r') as jsonFile:
#		data = json.load(jsonFile)
#		jsonFile.close()
#	ids = set([entry['resource']['id'] for entry in data['entry']])
#	for id in ids:
#		api.add_resource(Bundle, f'/Bundle/{id}', endpoint=f'Observation/{id}', resource_class_kwargs={'id':id})

# Endpoints
class Observation(Resource):
	def __init__(self, id):
		self.id = id
	def get(self):
		id = self.id
		# inp_path = '/mnt/c/Users/gilor/Documents/ETL/FHIR/FHIR-IMNA/Profiles/Observation/observation.json'
		inp_path = './fhir-api/imna_observations.json'
		with open(file=inp_path, mode='r') as jsonFile:
					data = json.load(jsonFile)
					jsonFile.close()
		if id:
			for entry in data['entry']:
				obs = entry['resource']
				if obs['id'] == self.id:
					return obs, 200 # return specific observation and 200 OK
		else:
			args = request.args
			if len(set(args.keys()) - {'_category','_status','_code','_id','_il-id','_pna-id','_ppn','_country','_date','_time'}) > 0 :
				return {}, 200 # return empty data and 200 OK

			if '_category' in args.keys():
				category = args['_category']
				entries = []
				for entry in data['entry']:
					obs = entry['resource']
					if checkResourceByCategory(obs, category):
						entries.append(entry)
				data.update({'entry': entries})

			if '_status' in args.keys():
				status = args['_status']
				entries = []
				for entry in data['entry']:
					obs = entry['resource']
					if obs['status'] == status:
						entries.append(entry)
				data.update({'entry': entries})

			if '_code' in args.keys():
				code = args['_code']
				entries = []
				for entry in data['entry']:
					obs = entry['resource']
					if checkResourceByCode(obs, code):
						entries.append(entry)
				data.update({'entry': entries})

			if '_id' in args.keys():
				id = args['_id']
				for entry in data['entry']:
					obs = entry['resource']
					if obs['id'] == id:
						return obs, 200 # return specific observation and 200 OK
				return {}, 200  # return empty data and 200 OK

			if ('_il-id' in args.keys()) | ('_pna-id' in args.keys()) | ('_ppn' in args.keys()):
				if '_il-id' in args.keys():
					identifier = 'il-id'
					patient_id = args['_il-id']
				elif '_pna-id' in args.keys():
					identifier = 'pna-id'
					patient_id = args['_pna-id']
				else:
					identifier = 'ppn'
					patient_id = args['_ppn']
				entries = []
				for entry in data['entry']:
					obs = entry['resource']
					if 'subject' in obs.keys():
						subject = obs['subject']
						if 'identifier' in subject.keys():
							identity = subject['identifier']
							if identifier in identity.keys():
								if identity[identifier]['value'] == patient_id:
									entries.append(entry)
					data.update({'entry': entries})

			if '_country' in args.keys():
				country = args['_country']
				entries = []
				for entry in data['entry']:
					obs = entry['resource']
					if 'subject' in obs.keys():
						subject = obs['subject']
						if 'identifier' in subject.keys():
							identity = subject['identifier']
							if 'ppn' in identity.keys():
								if identity['ppn']['system'].endswith(country):
									entries.append(entry)
				data.update({'entry': entries})

			if '_date' in args.keys():
				if '_time' not in args.keys():
					sep_count = args['_date'].count('-')
					if sep_count == 0:
						search_date = datetime.strptime(args['_date'], '%Y').date()
					elif sep_count == 1:
						search_date = datetime.strptime(args['_date'], '%Y-%m').date()
					else:
						search_date = datetime.strptime(args['_date'], '%Y-%m-%d').date()

					entries = []
					for entry in data['entry']:
						obs = entry['resource']
						if 'effectiveDateTime' in obs.keys():
							local_record_dt = datetime.fromisoformat(obs['effectiveDateTime'])
							utc_offset = local_record_dt.tzinfo.utcoffset(local_record_dt)
							utc_record_date = (utc_offset + local_record_dt).date()

							if sep_count == 0:
								record_date = datetime(year=utc_record_date.year, month=1, day=1).date()
							elif sep_count == 1:
								record_date = datetime(year=utc_record_date.year, month=utc_record_date.month,
													day=1).date()
							else:
								record_date = datetime(year=utc_record_date.year, month=utc_record_date.month, 
													day=utc_record_date.day).date()

							if search_date == record_date:
								entries.append(entry)
						elif 'effectivePeriod' in obs.keys():
							period = [datetime.fromisoformat(obs['effectivePeriod'][x]) for x in ['start','end']]
							utc_offsets = [dt.tzinfo.utcoffset(dt) for dt in period]
							start_date, end_date = [(utc_offset + local_dt).date() for utc_offset, local_dt in zip(utc_offsets, period)]

							if sep_count == 0:
								record__start_date = datetime(year=start_date.year, month=1, day=1)
								record__end_date = datetime(year=end_date.year, month=1, day=1)
							elif sep_count == 1:
								record__start_date = datetime(year=start_date.year, month=start_date.month, day=1)
								record__end_date = datetime(year=end_date.year, month=end_date.month, day=1)
							else:
								record__start_date = datetime(year=start_date.year, month=start_date.month, 
															day=start_date.day)
								record__end_date = datetime(year=end_date.year, month=end_date.month, 
															day=end_date.day)

							if record__start_date <= search_date <= record__end_date:
								entries.append(entry)
					data.update({'entry': entries})

				else:
					search_dt = args['_date']+ ' ' + args['_time']
					sep_count = search_dt.count(':')
					if sep_count == 0:
						search_dt = datetime.strptime(search_dt, '%Y-%m-%d %H')
					elif sep_count == 1:
						search_dt = datetime.strptime(search_dt, '%Y-%m-%d %H:%M')
					else:
						search_dt = datetime.strptime(search_dt, '%Y-%m-%d %H:%M:%S')
					entries = []
					for entry in data['entry']:
						obs = entry['resource']
						if 'effectiveDateTime' in obs.keys():
							local_record_dt = datetime.fromisoformat(obs['effectiveDateTime'])
							utc_offset = local_record_dt.tzinfo.utcoffset(local_record_dt)
							utc_record_dt = (utc_offset + local_record_dt)

							if sep_count == 0:
								record_dt = datetime(year=utc_record_dt.year, month=utc_record_dt.month, 
													day=utc_record_dt.day, hour=utc_record_dt.hour)
							elif sep_count == 1:
								record_dt = datetime(year=utc_record_dt.year, month=utc_record_dt.month, 
													day=utc_record_dt.day, hour=utc_record_dt.hour,
													minute=utc_record_dt.minute)
							else:
								record_dt = datetime(year=utc_record_dt.year, month=utc_record_dt.month, 
													day=utc_record_dt.day, hour=utc_record_dt.hour,
													minute=utc_record_dt.minute, second=utc_record_dt.second)

							if record_dt == search_dt:
									entries.append(entry)
						elif 'effectivePeriod' in obs.keys():
							period = [datetime.fromisoformat(obs['effectivePeriod'][x]) for x in ['start','end']]
							utc_offsets = [dt.tzinfo.utcoffset(dt) for dt in period]
							start_dt, end_dt = [(utc_offset + local_dt) for utc_offset, local_dt in zip(utc_offsets, period)]

							if sep_count == 0:
								record_start_dt = datetime(year=start_dt.year, month=start_dt.month, 
														day=start_dt.day, hour=start_dt.hour)
								record_end_dt = datetime(year=end_dt.year, month=end_dt.month, 
														day=end_dt.day, hour=end_dt.hour)
							elif sep_count == 1:
								record_start_dt = datetime(year=start_dt.year, month=start_dt.month, 
														day=start_dt.day, hour=start_dt.hour,
														minute=start_dt.minute)
								record_end_dt = datetime(year=end_dt.year, month=end_dt.month, 
														day=end_dt.day, hour=end_dt.hour,
														minute=end_dt.minute)
							else:
								record_start_dt = datetime(year=start_dt.year, month=start_dt.month, 
														day=start_dt.day, hour=start_dt.hour,
														minute=start_dt.minute, second=start_dt.second)
								record_end_dt = datetime(year=end_dt.year, month=end_dt.month, 
														day=end_dt.day, hour=end_dt.hour,
														minute=end_dt.minute, second=end_dt.second)

							if record_start_dt <= search_dt < record_end_dt:
								entries.append(entry)
					data.update({'entry': entries})
					
			if len(data['entry']) == 0:
				return {}, 200 # return empty data and 200 OK

			data['total'] = len(data['entry'])
			ts = datetime.now().astimezone()
			ts = ts - ts.tzinfo.utcoffset(ts)
			data['timestamp'] = ts.isoformat()
			data = {key: data[key] for key in ['resourceType','id','type','timestamp','total','entry']}
			return data, 200 # return queried data as a bundle and 200 OK

class Bundle(Resource):
	def __init__(self, id):
		self.id = id
	def get(self):
		id = self.id
		with open(file='/mnt/c/Users/gilor/Documents/ETL/FHIR/FHIR-IMNA/Profiles/Bundle/bundle.json', mode='r') as jsonFile:
					data = json.load(jsonFile)
					jsonFile.close()
		if id:
			for entry in data['entry']:
				bundle = entry['resource']
				if bundle['id'] == self.id:
					return bundle, 200 # return specific observation and 200 OK
		else:
			args = request.args
			if len(set(args.keys()) - {'_type','_id','_il-id','_pna-id','_ppn','_country'}) > 0:
				return {}, 200 # return empty data and 200 OK
			
			if '_type' in args.keys():
				type = args['_type']
				entries = []
				for entry in data['entry']:
					bundle = entry['resource']
					if bundle['type'] == type:
						entries.append(entry)
				data.update({'entry': entries})

			if '_id' in args.keys():
				id = args['_id']
				for entry in data['entry']:
					bundle = entry['resource']
					if bundle['id'] == id:
						return bundle, 200 # return specific observation and 200 OK
				return {}, 200  # return empty data and 200 OK

			if ('_il-id' in args.keys()) | ('_pna-id' in args.keys()) | ('_ppn' in args.keys()):
				if '_il-id' in args.keys():
					identifier = 'il-id'
					patient_id = args['_il-id']
				elif '_pna-id' in args.keys():
					identifier = 'pna-id'
					patient_id = args['_pna-id']
				else:
					identifier = 'ppn'
					patient_id = args['_ppn']
				entries = []
				for entry in data['entry']:
					for sub_entry in entry['resource']['entry']:
						resource =  sub_entry['resource']
						if 'subject' in resource.keys():
							subject = resource['subject']
							if 'identifier' in subject.keys():
								identity = subject['identifier']
								if identifier in identity.keys():
									if identity[identifier]['value'] == patient_id:
										entries.append(entry)
				data.update({'entry': entries})

			if '_country' in args.keys():
				country = args['_country']
				entries = []
				for entry in data['entry']:
					for sub_entry in entry['resource']['entry']:
						resource = sub_entry['resource']
						if 'subject' in resource.keys():
							subject = resource['subject']
							if 'identifier' in subject.keys():
								identity = subject['identifier']
								if 'ppn' in identity.keys():
									if identity['ppn']['system'].endswith(country):
										entries.append(entry)
				data.update({'entry': entries})

			if len(data['entry']) == 0:
				return {}, 200 # return empty data and 200 OK

			keys = data.keys()
			data['total'] = len(data['entry'])
			data['timestamp'] = str(datetime.now() - timedelta(hours=2)).replace(' ','T') + '+02:00'
			data = {key: data[key] for key in ['resourceType','id','type','timestamp','total','entry']}
			return data, 200 # return queried data as a bundle and 200 OK
			

if __name__ == '__main__':

	# Initialize Flask API
	app = Flask(__name__)
	api = Api(app, default_mediatype='application/fhir+json')

	print(os.getcwd())
	# link Observation class with the /Observation endpoint
	api.add_resource(Observation, '/Observation', endpoint='Observation', resource_class_kwargs={'id': None})
	addIdEndpoints(api)
	api.add_resource(Bundle, '/Bundle', endpoint='Bundle', resource_class_kwargs={'id': None})

	# run our Flask app
	app.run(port=5000)
