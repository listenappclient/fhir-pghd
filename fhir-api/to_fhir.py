import json
from rstr import xeger
from pprint import pprint
from datetime import datetime
from pymongo import MongoClient
import sqlalchemy

alpha23_map = {"US":"USA", "IL":"ISR", "AZ": "AZE"}

unit_map = {"mg/dL": {"code":"mg/dL", "unit":"milligram per deciliter"},
			"mmol/L": {"code":"mmol/L", "unit":"millimole per liter"},
			"Kilogram(kg)": {"code":"kg", "unit":"kilogram"},
			"Kg": {"code":"kg", "unit":"kilogram"},
			"kilogram": {"code":"kg", "unit":"kilogram"},
			"kg": {"code":"kg", "unit":"kilogram"},
			"Pound(Lbs)": {"code":"[lb_av]", "unit":"pound (US and British)"},
			"Pound(lb)": {"code":"[lb_av]", "unit":"pound (US and British)"},
			"Pounds": {"code":"[lb_av]", "unit":"pound (US and British)"},
			"lbs": {"code":"[lb_av]", "unit":"pound (US and British)"},
			"Lbs": {"code":"[lb_av]", "unit":"pound (US and British)"},
			"pound": {"code":"[lb_av]", "unit":"pound (US and British)"},
			"mmHg": {"code":"mm[Hg]", "unit":"millimeter of mercury"}}

qualifier_map = {"2 Hours Post Breakfast": {"code":"7221000175107", "display":"After breakfast"},
				"2 Hours Post Dinner": {"code":"1521000175104", "display":"After dinner"},
				"Bed time": {"code":"2546009", "display":"Night time"},
				"Fasting": {"code":"726054005", "display":"After fasting"},
				"Pre Dinner": {"code":"3071000175109", "display":"Before dinner"},
				"Pre lunch": {"code":"3091000175105", "display":"Before lunch"}}

activity_map = {"Cycling": {"code":"722101000000105", "display":"Cycling", "system":"http://snomed.info/sct"}, 
				"cycling": {"code":"722101000000105", "display":"Cycling", "system":"http://snomed.info/sct"},
				"HIIT": {"code":"454691000124100", "display":"High-intensity interval training", "system":"http://snomed.info/sct"},
				"hiit": {"code":"454691000124100", "display":"High-intensity interval training", "system":"http://snomed.info/sct"},
				"House Work": {"code":"272387007", "display":"Domestic activity", "system":"http://snomed.info/sct"},
				"house work": {"code":"272387007", "display":"Domestic activity", "system":"http://snomed.info/sct"},
				"Other": {"code":"LA9379-4", "display":"Other specified activity", "system":"http://loinc.org"},
				"other": {"code":"LA9379-4", "display":"Other specified activity", "system":"http://loinc.org"},
				"Running": {"code":"418060005", "display":"Running", "system":"http://snomed.info/sct"},
				"running": {"code":"418060005", "display":"Running", "system":"http://snomed.info/sct"},
				"Swimming": {"code":"20461001", "display":"Swimming", "system":"http://snomed.info/sct"},
				"swimming": {"code":"20461001", "display":"Swimming", "system":"http://snomed.info/sct"}, 
				"Walking": {"code":"129006008", "display":"Walking", "system":"http://snomed.info/sct"},
				"walking": {"code":"129006008", "display":"Walking", "system":"http://snomed.info/sct"},
				"Weight Lifting": {"code":"266741004", "display":"Muscle strength exercise", "system":"http://snomed.info/sct"},
				"weight lifting": {"code":"266741004", "display":"Muscle strength exercise", "system":"http://snomed.info/sct"},
				"Yoga": {"code":"Y93.42", "display":"Activity, yoga", "system": "http://hl7.org/fhir/sid/icd-10-cm"},
				"yoga": {"code":"Y93.42", "display":"Activity, yoga", "system": "http://hl7.org/fhir/sid/icd-10-cm"}}

# establish a MySQL db connection with sqlalchemy
sqlEngine = sqlalchemy.create_engine('mysql+pymysql://imna_dev:Bkwz4vXC9uAHp4Dj@imna-dev-rds-cluster.cluster-ctxvj34ylikn.eu-west-1.rds.amazonaws.com:3306/Dev_listenApp', pool_recycle=3600)
try:
	dbConnection = sqlEngine.connect()
	q = sqlalchemy.text('select id, social_security_number, country_of_origin, first_name, middle_name, last_name from patients')
	patients = dbConnection.execute(q).fetchall()
	patients = {p[0]: (p[1], p[2], p[3], p[4], p[5]) for p in patients}

except ValueError as vx:
	print("\n",vx)
except Exception as ex:   
	print("\n",ex)
finally:
	dbConnection.close()

observations = []
# establish a Mongo db connection with MongoClient
with MongoClient('mongodb+srv://backend:RCZhuuX37Fg%2A6fGj%25WZWaBM%40E@development-drhew.mongodb.net') as client:
	db = client['dev']
	measurement = db['measurement-reports']
	for code, display in zip(['29463-7','8302-2','39156-5','85354-9','15074-8'],
							['Body weight','Body height','Body mass index (BMI) [Ratio]','Blood pressure','Blood glucose concentration']):
		bypass_flag = False
		for doc in measurement.find({'code': code}):

			if ('components' not in doc.keys()) | ('patient_id' not in doc.keys()):
				continue
			
			components = doc['components']
			id = str(doc['_id'])
			patient_id = doc['patient_id']
			rec = {'fullUrl': f'http://127.0.0.1:5000/Observation/{id}', 'resource': {'resourceType': 'Observation'}}
			rec['resource']['id'] = id

			try:
				pat_identifier, pat_country, pat_first_name, pat_middle_name, pat_last_name = patients[patient_id]
				pat_full_name = ' '.join([name for name in [pat_first_name, pat_middle_name, pat_last_name] if name])
				if pat_country == 'IL':
					rec['resource']['subject'] = {'identifier': {'il-id': {'system': 'http://fhir.health.org.il/identifier/il-national-id',
																		'value': pat_identifier}},
																'type': 'Patient',
																'display': pat_full_name}
				else:
					rec['resource']['subject'] = {'identifier': {'ppn': {'type': {'coding': [{'system': 'http://terminology.hl7.org/CodeSystem/v2-0203',
																							'code': 'PPN',
																							'display': 'Passport number'}]}, 
																		'system': f'http://hl7.org/fhir/sid/passport-{alpha23_map[pat_country]}', 
																		'value': pat_identifier}},
												'type': 'Patient',
												'display': pat_full_name}
			except KeyError:
				pass

			if 'updated_at' in doc.keys():
				if isinstance(doc['updated_at'], datetime):
					rec['resource']['meta'] = {'lastUpdated': doc['updated_at'].astimezone().isoformat()}
			if 'status' not in doc.keys():
				rec['resource']['status'] = 'unknown'
			elif doc['status'] == 'unknown':
				rec['resource']['status'] = 'unknown'
			elif doc['status'] in ['COMPLETE','Completed','Final','final']:
				rec['resource']['status'] = 'final'
			elif doc['status'] == 'Expired':
				rec['resource']['status'] = 'cancelled'
			elif doc['status'] in ['Skipped','Snoozed']:
				rec['resource']['status'] = 'registered'

			if 'effective' in doc.keys():
				if isinstance(doc['effective'], datetime):
					rec['resource']['effectiveDateTime'] = doc['effective'].astimezone().isoformat()
			if 'note' in doc.keys():
				if not doc['note'] is None:
					if len(doc['note']) > 0:
						rec['resource']['note'] = {'text': doc['note']}

			elif 'comments' in doc.keys():
				if not doc['comments'] is None:
					if len(doc['comments']) > 0:
						rec['resource']['note'] = {'text': doc['comments']}

			if code == '15074-8':
				if len(components) == 0:
					continue
				else:
					component = components[0]

				value = component['value']
				if value is None:
					continue

				if 'display_name' in component.keys():
					if component['display_name'] not in ["Blood Sugar","Blood Sugar (Glucose)","Blood Sugar Reading","Blood Glucose","Glucose","Glucose - Blood Sugar"]:
						continue
				elif 'display' in component.keys():
					if component['display'] not in ["Blood Sugar","Blood Sugar (Glucose)","Blood Sugar Reading","Blood Glucose","Glucose","Glucose - Blood Sugar"]:
						continue
				else:
					continue

				rec['resource']['category'] = [{'coding': [{'system': 'http://terminology.hl7.org/CodeSystem/observation-category', 'code': 'laboratory', 'display': 'Laboratory'}]}]
				rec['resource']['code'] = {'coding': [{'system': 'http://snomed.info/sct', 'code': '434912009', 'display': display}]}

				if 'units' in component.keys():
					unit = component['units']
				elif 'units' in doc.keys():
					unit = doc['units']
				else:
					continue

				if isinstance(unit, list):
					unit = unit[0]

				if unit not in ["mg/dL","mmol/L"]:
					continue

				rec['resource']['valueQuantity'] = {'value': value, 'unit': unit_map[unit]['unit'], 'system': 'http://unitsofmeasure.org', 'code': unit_map[unit]['code']}

				rec['resource']['component'] = []
				if 'attributes' in doc.keys():
					for attr in doc['attributes']:
						if 'display' in attr.keys():
							if attr['display'] == 'Place':
								if attr['value'] == "At Lab":
									rec['method'] = {'coding': [{'system': 'http://snomed.info/sct', 'code': '15220000', 'display': 'Laboratory test'}]}
								elif attr['value'] == "With Glucometer":
									rec['method'] = {'coding': [{'system': 'http://snomed.info/sct', 'code': '337414009', 'display': 'Blood glucose meter'}]}
							elif (attr['display'] == 'Insulin(units)') & ('value' in attr.keys()):
								value = attr['value']
								if value.isdigit():
									rec['resource']['component'].append({'code': {'coding': [{'system': 'http://loinc.org','code': '20448-7', 'display': 'Insulin [Units/volume] in Serum or Plasma'}]},
					  										'valueQuantity': {'value': value, 'unit': 'milli international unit per liter','system': 'http://unitsofmeasure.org', 'code': 'm[IU]/L'}})
							elif (attr['display'] == 'Reading Type') & ('value' in attr.keys()):
								value = attr['value']
								rec['resource']['component'].append({'code': {'coding': [{'system': 'http://snomed.info/sct', 'code': '703763000', 'display': 'Precondition value'}]},
					  									'valueCodeableConcept': {'coding': [{'system': 'http://snomed.info/sct', 'code': qualifier_map[value]['code'], 'display': qualifier_map[value]['display']}]}})
				if len(rec['resource']['component']) == 0:
					del rec['resource']['component']	

			else:
				rec['resource']['category'] = [{'coding': [{'system': 'http://terminology.hl7.org/CodeSystem/observation-category', 'code': 'vital-signs', 'display': 'Vital Signs'}]}]
				rec['resource']['code'] = {'coding': [{'system': 'http://loinc.org', 'code': code, 'display': display}]}

				if isinstance(components, list):
					if len(components) == 0:
						continue
				elif isinstance(components, dict):
					components = [components['0']]
			
				if code == '85354-9':
					rec['resource']['component'] = []
					for comp in components:
						if 'component' in comp.keys():
							comp = comp['component']

						if 'display' in comp.keys():
							comp_display = comp['display']
						elif 'display_name' in comp.keys():
							comp_display = comp['display_name']
						else:
							continue

						if 'code' in comp.keys():
							comp_code = comp['code']
						elif 'loincCode' in comp.keys():
							comp_code = comp['loincCode']
						else:
							continue
					
						if comp_display in ['SYS','Systolic','Systolic Blood pressure']:
							comp_display = 'Systolic blood pressure'
						elif comp_display in ['DIA','Diastolic']:
							comp_display = 'Diastolic blood pressure'
						else:
							continue
						
						if 'units' in comp.keys():
							unit = comp['units']
						elif 'units' in doc.keys():
							unit = doc['units']
						else:
							continue
						
						if isinstance(unit, list):
							unit = unit[0]

						if unit != 'mmHg':
							bypass_flag = True
							break
						
						value = comp['value']
						if value is None:
							comp_dict = {'code': {'coding': [{'system': 'http://loinc.org', 'code': comp_code, 'display': comp_display}]},
										'dataAbsentReason': {'coding': [{'system': 'http://terminology.hl7.org/CodeSystem/data-absent-reason', 'code': 'unknown' , 'display': 'Unknown'}]}}
						else:
							comp_dict = {'code': {'coding': [{'system': 'http://loinc.org', 'code': comp_code, 'display': comp_display}]},
										'valueQuantity': {'value': value, 'unit': unit_map[unit]['unit'], 'system': 'http://unitsofmeasure.org', 'code': unit_map[unit]['code']}}

						rec['resource'].update({'component': rec['resource']['component'] + [comp_dict]})

					if (bypass_flag) | (len(rec['resource']['component']) == 0):
						continue
				else:
					component = components[0]
					if 'units' in component.keys():
						unit = component['units']
					elif 'units' in doc.keys():
						unit = doc['units']
					else:
						continue
					
					if isinstance(unit, list):
						unit = unit[0]

					value = component['value']
					if value is None:
						rec['resource']['dataAbsentReason'] = {'coding': [{'system': 'http://terminology.hl7.org/CodeSystem/data-absent-reason', 'code': 'unknown' , 'display': 'Unknown'}]}
					else:	
						rec['resource']['valueQuantity'] = {'value': value, 'unit': unit_map[unit]['unit'], 'system': 'http://unitsofmeasure.org', 'code': unit_map[unit]['code']}

			rec.update({'resource': {k:rec['resource'][k] for k in ['resourceType','id','meta','status','category','code','subject','effectiveDateTime','effectivePeriod','performer','valueQuantity','valueCodeableConcept','dataAbsentReason','note','method','component'] if k in rec['resource'].keys()}})

			observations.append(rec)

	activity_reports = db['activity-report']
	for doc in activity_reports.find({}):

		if ('activities' not in doc.keys()) | ('patient_id' not in doc.keys()):
			continue
		elif not doc['is_active']:
			continue

		activities = doc['activities']
		if len(activities) == 0:
			continue
		id = str(doc['_id'])
		patient_id = doc['patient_id']
		rec = {'fullUrl': f'http://127.0.0.1:5000/Observation/{id}', 'resource': {'resourceType': 'Observation'}}
		rec['resource']['id'] = id

		try:
			pat_identifier, pat_country, pat_first_name, pat_middle_name, pat_last_name = patients[patient_id]
			pat_full_name = ' '.join([name for name in [pat_first_name, pat_middle_name, pat_last_name] if name])
			if pat_country == 'IL':
				rec['resource']['subject'] = {'identifier': {'il-id': {'system': 'http://fhir.health.org.il/identifier/il-national-id',
																	'value': pat_identifier}},
															'type': 'Patient',
															'display': pat_full_name}
			else:
				rec['resource']['subject'] = {'identifier': {'ppn': {'type': {'coding': [{'system': 'http://terminology.hl7.org/CodeSystem/v2-0203',
																						'code': 'PPN',
																						'display': 'Passport number'}]}, 
																	'system': f'http://hl7.org/fhir/sid/passport-{alpha23_map[pat_country]}', 
																	'value': pat_identifier}},
											'type': 'Patient',
											'display': pat_full_name}
		except KeyError:
			pass

		if 'updated_at' in doc.keys():
			if isinstance(doc['updated_at'], datetime):
				update_dt = doc['updated_at'].astimezone().isoformat()
				rec['resource']['meta'] = {'lastUpdated': update_dt}
				rec['resource']['effectiveDateTime'] = update_dt
			elif isinstance(doc['created_at'], datetime):
				create_dt = doc['created_at'].astimezone().isoformat()
				rec['resource']['effectiveDateTime'] = create_dt
		rec['resource']['status'] = 'unknown'
		rec['resource']['category'] = [{'coding': [{'system': 'http://terminology.hl7.org/CodeSystem/observation-category', 'code': 'activity', 'display': 'Activity'}]}]
		rec['resource']['code'] = {'coding': [{'system': 'http://loinc.org', 'code': '82287-4', 'display': 'Physical activity panel'}]}

		components = []
		total_activity = 0
		for act in activities:
			act_value = act['value']
			if (act_value == 0) | ('name' not in act.keys()):
				continue
			act_name = act['name']
			if act_name:
				comp = {'code': {'coding': [{'system': activity_map[act_name]['system'], 'code': activity_map[act_name]['code'], 'display': activity_map[act_name]['display']}]},
						'valueQuantity': {'value': act_value, 'unit': 'minute', 'system': 'http://unitsofmeasure.org', 'code':'min'}}
				components.append(comp)
				total_activity += act_value

		if len(components) > 0:
			rec['resource']['component'] = components
			total_activity = round(total_activity / 60, 2)
			rec['resource']['valueQuantity'] = {'value': total_activity, 'unit': 'hour', 'system': 'http://unitsofmeasure.org', 'code':'h'}

			rec.update({'resource': {k:rec['resource'][k] for k in ['resourceType','id','meta','status','category','code','subject','effectiveDateTime','effectivePeriod','performer','valueQuantity','valueCodeableConcept','dataAbsentReason','note','method','component'] if k in rec['resource'].keys()}})
			observations.append(rec)
	
	# close connection
	client.close()


observations_bundle = {'resourceType': 'Bundle', 'id': xeger(r'^[A-Za-z0-9]{24}$'), 'type': 'searchset', 'entry': observations}
with open('/home/gil/scripts/fhir-api/imna_observations.json', mode='w') as outFile:
	json.dump(obj=observations_bundle, fp=outFile, indent=6)